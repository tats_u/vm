##################################################
# gcc5(g++5)・clang(clang++)が使えるならそちら優先
# compiler=norewrite を指定すると書き換えない
# 名古屋大学 RHEL/CentOS6用
##################################################

comchk = which COMPILER > /dev/null 2>&1 && echo COMPILER #バージョンチェック用オプション
ifneq ($(compiler),norewrite)
CC := $(shell $(comchk:COMPILER=clang) || ($(comchk:COMPILER=gcc5) || echo $(CC)))
CXX := $(shell $(comchk:COMPILER=clang++) || ($(comchk:COMPILER=g++5) || echo $(CXX)))
endif
