// コンパイラ演習仮想マシン実行プログラム
// C++ ver.

/*
  The MIT License (MIT)

  Copyright (c) 1992-1995 YUEN Shoji (Original)
                2015-2016 UCHINO Tatsunori (C++ refactoring)

  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

/*
 *
 *
 学生実験(コンパイラ) 仮想マシン実現系
	
 An Implementation for Gakusei jikken Compiler Virtual machine

 $Id: vm.c,v 2.3 1995/06/29 05:00:25 yuen Exp $

 $Log: vm.c,v $
 * Revision 2.3  1995/06/29  05:00:25  yuen
 * strings.h -> string.h
 *
 * Revision 2.2  1995/06/29  04:57:37  yuen
 * RTN simulation is not quite right
 *
 * Revision 2.1  1993/06/08  08:17:36  yuen
 * H5 version vm.c
 *
 * Revision 1.4  1993/06/08  08:15:47  yuen
 * Procedure call and return renewed
 *
 * Revision 1.3  1992/10/21  04:03:35  yuen
 * disassembler modified
 *
 * Revision 1.2  1992/10/21  02:46:16  yuen
 * debug mode is added.
 *
 * Revision 1.1  1992/10/20  09:38:16  yuen
 * Initial revision
 *
 */

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <vector>
#include <getopt.h>
#include <cstdlib>
#include <memory>
#include "machine.hpp"

bool vflag = false;             // 最後にスタックトップを出力する
bool dflag = false;
namespace S = std;

void show_help(const char* argv0) {
  using namespace S;
  cerr << "Usage: " << argv0 << " [OPTION] [objectfile]\n\
\t-d\t--debug\t\tDebug mode\n\
\t-v\t--verbose\tVerbose mode\n\
\t\t--help\t\tShow this help\n\
\t\t--version\tShow the version of this program\n\
";
}
void show_version(const char* argv0) {
  using namespace S;
  cerr << "Virtual machine\n";
}

int main(int argc, char *argv[]) {
  using namespace S;
  static struct option longopts[] = {
    { "verbose",    no_argument,       NULL, 'v' },
    { "debug",  no_argument,       NULL, 'd' },
    { "help", no_argument, NULL, 1 },
    { "version", no_argument, NULL, 2 },
    { 0,        0,                 0,     0  },
  };
  int opt;
  int longindex;
  while((opt = getopt_long(argc, argv, "vd", longopts, &longindex)) != -1) {
    switch(opt) {
    case 1:
      show_help(argv[0]);
      exit(0);
    case 2:
      show_version(argv[0]);
      exit(0);
      break;
    case 'v':
      vflag = true;
      break;
    case 'd':
      dflag = true;
      break;
    default:
      show_help(argv[0]);
      exit(2);
      break;
    }
  }

  istream *is;
  ifstream ifs;
  if(optind + 1 == argc) {
    ifs.open(argv[optind]);
    if(!ifs) {
      cerr << "エラー: オブジェクトファイル「" << argv[optind] << "」が開けませんでした\n";
      exit(1);
    }
    is = &ifs;
  } else if(optind == argc) {
    is = &cin;
  } else {
    cerr << "エラー: 実行するオブジェクトファイルは1つしか指定できません\n";
    return 1;
    exit(1);
  }
  try {
    vmachine::vmachine vm(*is,dflag,vflag);
    return vm.run();  
  } catch(exception &e) {
    cerr << "例外発生:\n" << e.what() << endl;
    return 1;
  } catch(...) {return -1;}
}
