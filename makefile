include compiler_rewrite.mk
include detect_stdarch.mk
CXXFLAGS += -O3
ifeq ($(debug),on)
CXXFLAGS += -ggdb -g3
else
CXXFLAGS += -O3
endif
CXXFLAGS += $(cxxstd) $(arch)


vm: vm.cpp machine.cpp machine.hpp
	$(CXX) vm.cpp machine.cpp -o vm $(CXXFLAGS)
.PHONY: clean
clean:
	$(RM) vm
