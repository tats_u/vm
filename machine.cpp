#include <iostream>
#include <sstream>
#include <algorithm>
#include "machine.hpp"

namespace vmachine {
  vmachine::vmachine(istream &_is, bool D, bool V)
    : _d(D), _v(V),is(_is)      // -d・-vフラグ/入力ストリーム
    , rgst({0,0,-1,0,0})        // レジスタ初期値
    , PC(0), instPC(0)          // PC(プログラムカウンタ)
    , R1(rgst[1]),R2(rgst[2]),R3(rgst[3]),R4(rgst[4]) // 各レジスタのエイリアス
    , inst_arr({                                      // 各命令の関数ポインタへの配列
        &vmachine::exec_lit,&vmachine::exec_lod,&vmachine::exec_sto,&vmachine::exec_mvx
          ,&vmachine::exec_opr,&vmachine::exec_int,&vmachine::exec_cal,&vmachine::exec_rtn
          ,&vmachine::exec_jmp,&vmachine::exec_jpc,&vmachine::exec_get,&vmachine::exec_put
          ,&vmachine::exec_getc,&vmachine::exec_putc
          })
    , opr_arr({                 // OPR命令の種類の配列
        &vmachine::exec_opr_s_inv,&vmachine::exec_opr_add,&vmachine::exec_opr_sub,&vmachine::exec_opr_mul
          ,&vmachine::exec_opr_div,&vmachine::exec_opr_seq,&vmachine::exec_opr_sne,&vmachine::exec_opr_slt
          ,&vmachine::exec_opr_sleq,&vmachine::exec_opr_sgt,&vmachine::exec_opr_sgeq
      })
    , inst_pr_arr({             // 表示用
        &vmachine::print_nemo_lit,&vmachine::print_nemo_lod,&vmachine::print_nemo_sto,&vmachine::print_nemo_mvx
          ,&vmachine::print_nemo_opr,&vmachine::print_nemo_int,&vmachine::print_nemo_cal,&vmachine::print_nemo_rtn
          ,&vmachine::print_nemo_jmp,&vmachine::print_nemo_jpc,&vmachine::print_nemo_get,&vmachine::print_nemo_put
          ,&vmachine::print_nemo_getc,&vmachine::print_nemo_putc
          })
    , opr_pr_arr({              // 表示用
        &vmachine::print_opst_opr_s_inv,&vmachine::print_opst_opr_add,&vmachine::print_opst_opr_sub,&vmachine::print_opst_opr_mul
          ,&vmachine::print_opst_opr_div,&vmachine::print_opst_opr_seq,&vmachine::print_opst_opr_sne,&vmachine::print_opst_opr_slt
          ,&vmachine::print_opst_opr_sleq,&vmachine::print_opst_opr_sgt,&vmachine::print_opst_opr_sgeq
      }){
    load(_is);
  }

  void vmachine::set_d_flg(bool D) {
    _d = D;
  }

  void vmachine::set_v_flg(bool V) {
    _v = V;
  }

  // 別のファイルを読み込み
  void vmachine::reload(istream &_is) {
    txt_seg.clear();
    reset();
    load(_is);
  }

  // 仮想マシンの状態をリセットする
  void vmachine::reset() {
    R1 = 0;
    R2 = -1;
    R3 = 0;
    R4 = 0;
    PC = 0;
    instPC = 0;
  }

  // 入力ストリームを指定して命令列を読み込み
  void vmachine::load(istream &_is) {
    OPCODE one_ins;
    while(true) {               // 命令を全てtxt_segに読み込み
      is.read((char*)&one_ins,sizeof(OPCODE));
      if(is.eof()) break;
      if(is.fail()) {
        throw runtime_error("コードが壊れています");
      }
      if(is.bad()) {
        throw runtime_error("実行中に読み込みエラー発生");
      }
      txt_seg.push_back(one_ins);
    }
  }

  // 命令を全て実行する(途中の場合は最初から)
  int vmachine::run() {
    while(PC >= 0 && PC < txt_seg.size()) {
      fetch_exec(txt_seg.at(PC));
    }
    cout << "---実行終了---\n";
    if(_v) {
      print_stacktop(cout);
    }
    if(PC != txt_seg.size()) {
      cerr << "【警告】コードの終了地点ではありません！(PC = " << PC << ")\n";
    }
    return 0;
  }

  void vmachine::stepin() {
    if(PC >= txt_seg.size()) {
      return;
    }
  }

  void vmachine::stepover() {
    if(PC >= txt_seg.size()) {
      return;
    }
    for(int step = 1; step > 1 && PC < txt_seg.size(); ) {
      
    }
  }

  void vmachine::stepout() {
    if(PC >= txt_seg.size()) {
      return;
    }
    for(int step = 1; step >= 1 && PC < txt_seg.size(); ) {
      break;
    }
  }

  // スタックの指定番地を読み込み(stack[x]のラッパ)
  REG vmachine::stack_read(const REG &x) {
    if(x < 0) {
      stringstream errmes;
      errmes << "スタックのマイナスの領域から読み込まれようとしました(PC = " << instPC << "; 場所 = " << x << ")\n\n付近のコード: \n";
      print_code(errmes,instPC - 2, instPC + 2);
      throw out_of_range(errmes.str());
    }
    stack.reserve(x + 1);
    if(x >= stack.size()) stack.resize(x + 1, 0);
    return stack[x];
    // return stack.at(x);
  }

  // スタックの指定番地に書き込み(stack[x] = ...のラッパ)
  void vmachine::stack_write(const REG &x, const REG &val) {
    if(x < 0) {
      stringstream errmes;
      errmes << "スタックのマイナスの領域に書き込まれようとしました(PC = " << instPC << "; 場所 = " << x << "; 値 = " << val << ")\n\n付近のコード: \n";
      print_code(errmes, instPC - 2, instPC + 2);
      throw out_of_range(errmes.str());
    }
    if(x >= stack.size()) {
      stack.reserve(x + 1);
      stack.resize(x + 1, 0);
    }
    stack[x] = val;
  }

  // 1命令を実行
  void vmachine::fetch_exec(const OPCODE &inst) {
    if(inst.opcode < 0 || inst.opcode >= inst_num) {
      stringstream errmes;
      errmes << "不正な命令を検出しました。(";
      print_inst(errmes,inst);
      errmes << " @ PC = " << PC << ")\n\n付近のコード:\n";
      print_code(errmes, PC - 2, PC + 2);
      throw invalid_argument(errmes.str());
    }
    if(_d) cerr << "PC = " << PC << endl;
    instPC = PC++;
    (this->*(this->inst_arr[inst.opcode]))(inst);
    if(_d) {
      print_inst(cerr, inst);
      cerr << endl;
      print_state(cerr);
    }
  }

  // 出力ストリームに命令の出力(CUIアプリ用)
  void vmachine::print_inst(ostream &_os, const OPCODE &inst) {
    if(inst.opcode >= 0 && inst.opcode < inst_num) {
      (this->*(this->inst_pr_arr[inst.opcode]))(_os, inst);
    } else {
      string nemo_num = "?\?\?(#";
      nemo_num += to_string(inst.opcode);
      nemo_num += ')';
      print_nemo_allarg(_os,nemo_num, inst);
    }
  }
  void vmachine::exec_lit(const OPCODE &inst) {
    stack_write(++R2, inst.address);
  }
  void vmachine::exec_lod(const OPCODE &inst) {
    ++R2;
    stack_write(R2, stack_read(rgst.at(inst.basereg) + rgst.at(inst.indexreg) + inst.address));
  }
  void vmachine::exec_sto( const OPCODE &inst) {
    stack_write(rgst.at(inst.basereg) + rgst.at(inst.indexreg) + inst.address, stack.at(R2));
    --R2;
  }
  void vmachine::exec_mvx(const OPCODE &inst) {
    if(inst.indexreg >= 1 && inst.indexreg <= 4) {
      rgst[inst.indexreg] = stack_read(R2--);
    } else {
      stringstream errmes;
      errmes << "mvx命令: iの値が不正(" << inst.indexreg << ")です(PC = " << instPC << ")\n\n付近のコード:\n";
      print_code(errmes,instPC - 2, instPC + 2);
      throw invalid_argument(errmes.str());
    }
  }
  void vmachine::exec_opr(const OPCODE &inst) {
    if(inst.address < 0 || inst.address >= opr_inst_num) {
      stringstream errmes;
      errmes << "不正な(a = " << inst.address << ")opr命令を検出しました。(PC = " << instPC << "; a = #" << inst.address << ")\n\n付近のコード:\n";
      print_code(errmes,instPC - 2, instPC + 2);
      throw invalid_argument(errmes.str());
    }
    (this->*(this->opr_arr[inst.address]))();
    if(_d) {
      cerr << "PC = " << instPC << "; REG = {" << R1 << ", " << R2 << ", " << R3 << ", " << R4 << "}\n";
    }
  }
  void vmachine::print_nemo_noarg(ostream &_os, const string &opname, const OPCODE &inst) {
    _os << opname << " (" << inst.basereg << "), (" << inst.indexreg <<"), (" << inst.address << ')';
  }
  void vmachine::print_nemo_aonly(ostream &_os, const string &opname, const OPCODE &inst) {
    _os << opname << " (" << inst.basereg << "), (" << inst.indexreg <<"), " << inst.address;
  }
  void vmachine::print_nemo_ionly(ostream &_os, const string &opname, const OPCODE &inst) {
    _os << opname << " (" << inst.basereg << "), " << inst.indexreg <<", (" << inst.address << ')';
  }
  void vmachine::print_nemo_allarg(ostream &_os, const string &opname, const OPCODE &inst) {
    _os << opname << " " << inst.basereg << ", " << inst.indexreg <<", " << inst.address;
  }
  void vmachine::print_nemo_lit(ostream &_os, const OPCODE &inst) {print_nemo_aonly(_os,"LIT",inst);}
  void vmachine::print_nemo_lod(ostream &_os, const OPCODE &inst) {print_nemo_allarg(_os,"LOD",inst);}
  void vmachine::print_nemo_sto(ostream &_os, const OPCODE &inst) {print_nemo_allarg(_os,"STO",inst);}
  void vmachine::print_nemo_mvx(ostream &_os, const OPCODE &inst) {print_nemo_ionly(_os,"MVX",inst);}
  void vmachine::print_nemo_opr(ostream &_os, const OPCODE &inst) {
    print_nemo_aonly(_os,"OPR",inst);
    if(inst.address < opr_inst_num)
      (this->*(this->opr_pr_arr[inst.address]))(_os);
    else
      print_opst_opr_unknown(_os);
  }
  void vmachine::exec_opr_s_inv() {
    stack_write(R2, -stack_read(R2));
  }
  void vmachine::exec_opr_add() {
    const REG b = stack_read(R2--);
    const REG a = stack_read(R2);
    const REG res = a + b;
    if(_d && (a > 0) == (b > 0) && (a > 0) != (res > 0)) {
      cerr << "opr命令(加算): オーバーフローを検出しました。(PC = " << instPC << ")\n";
    }
    stack_write(R2, res);
  }
  void vmachine::exec_opr_sub() {
    const REG b = stack_read(R2--);
    const REG a = stack_read(R2);
    const REG res  = a - b;
    if(_d && (a > 0) != (b > 0) && (a > 0) != (res > 0)) {
      cerr << "opr命令(減算): オーバーフローを検出しました。(PC = " << instPC << ")\n";
    }
    stack_write(R2, res);
  }
  void vmachine::exec_opr_mul() {
    const REG b = stack_read(R2--);
    const REG a = stack_read(R2);
    const REG res = a * b;
    if(_d && res != 0) {
      const REG A = abs(a);
      const REG B = abs(b);
      const REG RES = abs(res);
      if(max(A,B) > RES) {
        cerr << "opr命令(乗算): オーバーフローを検出しました。(PC = " << instPC << ")\n";
      }
    }
    stack_write(R2, res);
  }
  void vmachine::exec_opr_div() {
    const REG b = stack_read(R2--);
    if(b != 0) {
      const REG a = stack_read(R2);
      stack_write(R2, a / b);
    } else {
      stringstream errmes;
      errmes << "opr命令(除算): ゼロ除算を検出しました。(PC = " << instPC << ")\n\n付近のコード: \n";
      print_code(errmes, instPC - 2, instPC + 2);
      throw invalid_argument(errmes.str());
    }
  }
  void vmachine::exec_opr_seq() {
    const REG b = stack_read(R2--);
    const REG a = stack_read(R2);
    stack_write(R2, a == b ? 1 : 0);
  }
  void vmachine::exec_opr_sne() {
    const REG b = stack_read(R2--);
    const REG a = stack_read(R2);
    stack_write(R2, a != b ? 1 : 0);
  }
  void vmachine::exec_opr_slt() {
    const REG b = stack_read(R2--);
    const REG a = stack_read(R2);
    stack_write(R2, a < b ? 1 : 0);
  }
  void vmachine::exec_opr_sleq() {
    const REG b = stack_read(R2--);
    const REG a = stack_read(R2);
    stack_write(R2, a <= b ? 1 : 0);
  }
  void vmachine::exec_opr_sgt() {
    const REG b = stack_read(R2--);
    const REG a = stack_read(R2);
    stack_write(R2, a > b ? 1 : 0);
  }
  void vmachine::exec_opr_sgeq() {
    const REG b = stack_read(R2--);
    const REG a = stack_read(R2);
    stack_write(R2, a >= b ? 1 : 0);
  }
  void vmachine::exec_int(const OPCODE &inst) {
    R2 += inst.address;
  }
  void vmachine::exec_cal(const OPCODE &inst) {
    stack_write(++R2, R1);
    stack_write(++R2, PC);
    stack_write(++R2, R3);
    stack_write(++R2, R4);
    R1 = R2 + 1;
    PC = inst.address;
  }
  void vmachine::exec_rtn(const OPCODE &inst) {
    R2 = R1 - 1;
    R4 = stack_read(R2--);
    R3 = stack_read(R2--);
    PC = stack_read(R2--);
    R1 = stack_read(R2--);
  }
  void vmachine::exec_jmp(const OPCODE &inst) {
    PC = inst.address;
  }
  void vmachine::exec_jpc(const OPCODE &inst) {
    if(stack_read(R2--) == 0) PC = inst.address;
  }
  void vmachine::exec_get(const OPCODE &inst) {
    REG n;
    cerr << "> ";
    cin >> n;
    stack_write(++R2, n);
  }
  void vmachine::exec_put(const OPCODE &inst) {
    cout << stack_read(R2--) << '\n';
  }
  void vmachine::exec_getc(const OPCODE &inst) {
    char ch;
    cerr << "> ";
    cin >> ch;
    stack_write(++R2, (REG)(unsigned char)ch);
  }
  void vmachine::exec_putc(const OPCODE &inst) {
    char ch = stack_read(R2--);
    cout << ch;
  }
  void vmachine::print_opst_opr_s_inv(ostream &_os) {_os << "(×(-1))";}
  void vmachine::print_opst_opr_add(ostream &_os) {_os << "(+)";}
  void vmachine::print_opst_opr_sub(ostream &_os) {_os << "(-)";}
  void vmachine::print_opst_opr_mul(ostream &_os) {_os << "(×)";}
  void vmachine::print_opst_opr_div(ostream &_os) {_os << "(÷)";}
  void vmachine::print_opst_opr_seq(ostream &_os) {_os << "(=)";}
  void vmachine::print_opst_opr_sne(ostream &_os) {_os << "(≠)";}
  void vmachine::print_opst_opr_slt(ostream &_os) {_os << "(<)";}
  void vmachine::print_opst_opr_sleq(ostream &_os) {_os << "(≦)";}
  void vmachine::print_opst_opr_sgt(ostream &_os) {_os << "(>)";}
  void vmachine::print_opst_opr_sgeq(ostream &_os) {_os << "(≧)";}
  void vmachine::print_opst_opr_unknown(ostream &_os){_os << "(?\?\?)";}
  void vmachine::print_nemo_int(ostream &_os, const OPCODE &inst) {print_nemo_aonly(_os,"INT",inst);}
  void vmachine::print_nemo_cal(ostream &_os, const OPCODE &inst) {print_nemo_aonly(_os,"CAL",inst);}
  void vmachine::print_nemo_rtn(ostream &_os, const OPCODE &inst) {print_nemo_noarg(_os,"RTN",inst);}
  void vmachine::print_nemo_jmp(ostream &_os, const OPCODE &inst) {print_nemo_aonly(_os,"JMP",inst);}
  void vmachine::print_nemo_jpc(ostream &_os, const OPCODE &inst) {print_nemo_aonly(_os,"JPC",inst);}
  void vmachine::print_nemo_get(ostream &_os, const OPCODE &inst) {print_nemo_noarg(_os,"GET",inst);}
  void vmachine::print_nemo_put(ostream &_os, const OPCODE &inst){print_nemo_noarg(_os,"PUT",inst);}
  void vmachine::print_nemo_getc(ostream &_os, const OPCODE &inst){print_nemo_noarg(_os,"GTC",inst);}
  void vmachine::print_nemo_putc(ostream &_os, const OPCODE &inst){print_nemo_noarg(_os,"PTC",inst);}
  void vmachine::print_state(ostream &_os) {
    _os << "PC = " << PC << "; レジスタ = {R1 = " << R1 << ", R2 = " << R2 << ", R3 = " << R3 << ", R4 = " << R4 << "}\n"
        << "スタック: ";
    if(R2 >= 0) {
      _os << '[';
      for(REG i = 0; i < R2; ++i) _os << stack_read(i) << ", ";
      _os << stack_read(R2) << ']' << '\n'; 
    } else if(R2 == -1) {
      _os << "(空)\n";
    } else {
      _os << "(壊れています)\n";
    }
    _os << endl;
  }
  void vmachine::print_stacktop(ostream &_os) {
    if(R2 >= 0) {
      _os << "スタックは空ではありません(一番上: " << stack_read(R2) << "; R2 = " << R2 << ")" << endl;
    } else if(R2 == -1){
      _os << "スタックは空です(R2 = -1)" << endl;
    } else {
      _os << "【警告】スタックポインタがおかしな位置(R2 = " << R2 << ")にあります！(R2 < -1)" << endl;
    }
  }
  void vmachine::print_code(ostream &os, REG from, REG to) {
    streamsize ndigit = [](streamsize len) {
      streamsize ret = 1;
      while((len /= 10) != 0) ++ret;
      return ret;
    }(txt_seg.size());
    from = max((REG)0,from);
    to = min(to, (REG)(txt_seg.size() - 1));
    if(from > to) swap(from,to);
    for(auto i = from; i <= to; ++i) {
      os.width(ndigit);
      os << i << ' ';
      os.width(0);
      print_inst(os,txt_seg[i]);
      os << '\n';
    }
  }
}
