##################################################
# コンパイラで指定できるなるべく最新のC/C++規格を使うコンパイラオプションを生成
# ついでにSandyBridgeまでのCPUでコンパイラで指定できるなるべく最新のCPU用の実行コードを生成するコンパイラオプションを生成
# 名古屋大学 RHEL/CentOS6用
##################################################

optchk = OPT -E -x LANG /dev/null >/dev/null 2>&1 && echo OPT #バージョンチェック用オプション
coptchk := $(optchk:LANG=c)
cxxoptchk := $(optchk:LANG=c++)
cstdchk := $(CC) $(coptchk:OPT=-std=VAL)
archchk := $(CC) $(coptchk:OPT=-march=VAL)
cxxstdchk := $(CXX) $(cxxoptchk:OPT=-std=VAL)

cstd := $(shell $(cstdchk:VAL=gnu11) || ($(cstdchk:VAL=gnu1x) || ($(cstdchk:VAL=gnu99) || echo))) #なるべく最新のC規格を使うオプション
ifneq (,$(findstring nagoya-u.ac.jp,$(shell hostname))) #学内でコンパイル
arch := $(shell $(archchk:VAL=corei7-avx) || ($(archchk:VAL=corei7) || ($(archchk:VAL=core2) || echo))) #SandyBridge(SSHサーバ)までのなるべく最新のIntel CPUに最適化
else
arch := -march=native
endif
cxxstd := $(shell $(cxxstdchk:VAL=gnu++17) || ($(cxxstdchk:VAL=gnu++1z) || ($(cxxstdchk:VAL=gnu++14) || ($(cxxstdchk:VAL=gnu++1y) || ($(cxxstdchk:VAL=gnu++11) || ($(cxxstdchk:VAL=gnu++0x) || echo)))))) #なるべく最新のC++規格を使うオプション
