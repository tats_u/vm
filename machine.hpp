#ifndef _MACHINE_HPP_
#define _MACHINE_HPP_
#include <array>
#include <vector>
namespace vmachine {
  using REG = int;
  
  struct OPCODE{	/* A Word type for OPeration CODE */
    int opcode;		/* Operation number */
    REG basereg;	/* Base register */
    REG indexreg;	/* Index register */
    REG address;	/* Address */
  };
  
  using namespace std;

  class vmachine {
  protected:
    bool _d;
    bool _v;
    istream &is;
    array<REG, 5> rgst;              // #0→ゼロレジスタ #1→フレームポインタ #2→スタックポインタ
    REG &R1,&R2,&R3,&R4;
    REG PC, instPC;
    vector<OPCODE> txt_seg;
    vector<REG> stack;
    static constexpr size_t inst_num = 14;
    static constexpr size_t opr_inst_num = 11;
    REG stack_read(const REG &x);
    void load(istream &);
    void stack_write(const REG &x, const REG &val);
    void fetch_exec(const OPCODE &inst);
    void exec_lit(const OPCODE &inst);
    void exec_lod(const OPCODE &inst);
    void exec_sto(const OPCODE &inst);
    void exec_mvx(const OPCODE &inst);
    void exec_opr(const OPCODE &inst);
    void print_nemo_noarg(ostream &_os, const string &opname, const OPCODE &inst);
    void print_nemo_aonly(ostream &_os, const string &opname, const OPCODE &inst);
    void print_nemo_ionly(ostream &_os, const string &opname, const OPCODE &inst);
    void print_nemo_allarg(ostream &_os, const string &opname, const OPCODE &inst);
    void print_nemo_lit(ostream &_os, const OPCODE &inst);
    void print_nemo_lod(ostream &_os, const OPCODE &inst);
    void print_nemo_sto(ostream &_os, const OPCODE &inst);
    void print_nemo_mvx(ostream &_os, const OPCODE &inst);
    void print_nemo_opr(ostream &_os, const OPCODE &inst);
    void exec_opr_s_inv();
    void exec_opr_add();
    void exec_opr_sub();
    void exec_opr_mul();
    void exec_opr_div();
    void exec_opr_seq();
    void exec_opr_sne();
    void exec_opr_slt();
    void exec_opr_sleq();
    void exec_opr_sgt();
    void exec_opr_sgeq();
    void print_opst_opr_s_inv(ostream &_os);
    void print_opst_opr_add(ostream &_os);
    void print_opst_opr_sub(ostream &_os);
    void print_opst_opr_mul(ostream &_os);
    void print_opst_opr_div(ostream &_os);
    void print_opst_opr_seq(ostream &_os);
    void print_opst_opr_sne(ostream &_os);
    void print_opst_opr_slt(ostream &_os);
    void print_opst_opr_sleq(ostream &_os);
    void print_opst_opr_sgt(ostream &_os);
    void print_opst_opr_sgeq(ostream &_os);
    void print_opst_opr_unknown(ostream &_os);
    void exec_int(const OPCODE &inst);
    void exec_cal(const OPCODE &insta);
    void exec_rtn(const OPCODE &inst);
    void exec_jmp(const OPCODE &inst);
    void exec_jpc(const OPCODE &insta);
    void exec_get(const OPCODE &inst);
    void exec_put(const OPCODE &inst);
    void exec_getc(const OPCODE &inst);
    void exec_putc(const OPCODE &inst);
    void print_nemo_int(ostream &_os, const OPCODE &inst);
    void print_nemo_cal(ostream &_os, const OPCODE &inst);
    void print_nemo_rtn(ostream &_os, const OPCODE &inst);
    void print_nemo_jmp(ostream &_os, const OPCODE &inst);
    void print_nemo_jpc(ostream &_os, const OPCODE &inst);
    void print_nemo_get(ostream &_os, const OPCODE &inst);
    void print_nemo_put(ostream &_os, const OPCODE &inst);
    void print_nemo_getc(ostream &_os, const OPCODE &inst);
    void print_nemo_putc(ostream &_os, const OPCODE &inst);
    array<void (vmachine::*)(const OPCODE &inst),inst_num> inst_arr;
    array<void (vmachine::*)(ostream&,const OPCODE &inst), inst_num> inst_pr_arr;
    array<void (vmachine::*)(),opr_inst_num> opr_arr;
    array<void (vmachine::*)(ostream&),opr_inst_num> opr_pr_arr;
    void print_code(ostream&, REG, REG);
  public:
    vmachine(istream &_is, bool D, bool V);
    void set_d_flg(bool);
    void set_v_flg(bool);
    void reload(istream&);
    void reset();
    int run();
    void stepin();
    void stepover();
    void stepout();
    void print_state(ostream &_os = cerr);
    void print_stacktop(ostream &_os = cerr);
    void print_inst(ostream &_os, const OPCODE &inst);
  };
}
#endif
