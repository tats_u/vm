/*
	Instruction code  Sccs(@(#)code.h	1.1 1/24/91)
*/
#ifndef _CODE_HPP_
#define _CODE_HPP_
#include "machine.hpp"
namespace vmachine {
  constexpr int LIT = 0;	/* Load Literal */
  constexpr int LOD = 1;	/* Load */
  constexpr int STO = 2;	/* Store */
  constexpr int MVX = 3;	/* Move Index Register */
  constexpr int OPR = 4;	/* Arithmetic operation */
  constexpr int INT = 5;	/* Increment stack top */
  constexpr int CAL = 6;	/* Procedure call */
  constexpr int RTN = 7;	/* Return */
  constexpr int JMP = 8;	/* Jump */
  constexpr int JPC = 9;	/* Jump on condition */
  constexpr int GET = 10;	/* Read data */
  constexpr int PUT = 11;	/* Write data */
  constexpr int GETC = 12;	/* Get a character */
  constexpr int PUTC = 13;	/* Put a character */

  constexpr REG O_SINV = 0;
  constexpr REG O_ADD = 1;
  constexpr REG O_SUB = 2;
  constexpr REG O_MUL = 3;
  constexpr REG O_DIV = 4;
  constexpr REG O_SEQ = 5;
  constexpr REG O_SNE = 6;
  constexpr REG O_SLT = 7;
  constexpr REG O_SLEQ = 8;
  constexpr REG O_SGT = 9;
  constexpr REG O_SGEQ = 10;

}
#endif
